# IT Load Marketplace Connector

## Description
The IT Load Marketplace Connector (ITMC) provides an API for Data Centres in order to trade load migrations in the CATALYST IT Load Marketplace. ITMC receives requests for potential load migrations or potential available resources that can execute remote loads, which formulates into market actions and posts them on the CATALYST IT Load Marketplace. After market clearance, ITMC receives the clearing result of the placed actions and informs accordingly the Data Centre Migration Controller; this component will execute live migration in case of approval or will cancel the migration in case of rejection of the market action.

## API
### Register candidate migration actions
| **URL**             |	/migration/{migration_id}/actions/{form}/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json             |
| **Request body**	  | MigrationAction                            |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Everything went well <br> 400 – The provided data is invalid or malformed <br> 500 – Internal server error |

###	Register candidate migration actions for specific marketplace timeframe
| **URL**             |	/migration/{migration_id}/actions/{form}/{timeframe}/ |
| ------------------- | ----------------------------------------------------- | 
| **Method**          |	POST                                                  |
| **Headers**         | Content-Type: application/json                        |
| **Request body**	  | MigrationAction                                       |
| **Response body**   | N/A                                                   |
| **Response codes**  | 201 – Everything went well <br> 400 – The provided data is invalid or malformed <br> 500 – Internal server error |

###	Retrieve reference prices
| **URL**             |	/prices/{form}/{timeframe}/ |
| ------------------- | ---------------------------- | 
| **Method**          |	GET                          |
| **Headers**         | Accept: application/json     |
| **Request body**	  | N/A                          |
| **Response body**	  | Price                        |
| **Response codes**  | 200 – Everything went well <br> 404 – No record was found matching the provided criteria <br> 500 – Internal server error |

### Retrieve reference prices for a given date
| **URL**             |	/prices/{form}/{timeframe}/date/{date}/ |
| ------------------- | --------------------------------------- |
| **Method**          |	GET                                     |
| **Headers**         | Accept: application/json                |
| **Request body**	  | N/A                                     |
| **Response body**	  | Price                                   |
| **Response codes**  | 200 – Everything went well <br> 404 – No record was found matching the provided criteria <br> 500 – Internal server error |

### Register migration status updates
| **URL**             |	/actions/<action_id>/status/   |
| ------------------- | ------------------------------ | 
| **Method**          |	POST                           |
| **Headers**         | Content-Type: application/json |
| **Request body**	  | ActionStatus                   |
| **Response body**   | N/A                            |
| **Response codes**  | 201 – Everything went well <br> 400 – The provided data is invalid or malformed <br> 500 – Internal server error |
  
  

### Data Model
**MigrationAction**
```json 
{
	"date": "2019-09-02T07:00:00Z",
	"starttime": "2019-09-03T01:00:00Z",
	"endtime": "2019-09-03T02:00:00Z",
	"loadvalues": [{
		"parameter": "cpu",
		"value": 30,
		"uom": "cpu"
	}, {
		"parameter": "ram",
		"value": 20480,
		"uom": "MB"
	}, {
		"parameter": "disk",
		"value": 100,
		"uom": "GB"
	}],
	"value": 0,
	"uom": "kWh",
	"price": 105,
	"action_type": "bid"
}
```

**ActionStatus**
```json 
{
	"status": "valid"
}
```

**Price**
```json 
{
	"price": 100.0
}
```

### Parameters
| **Parameter** | **Type** | **Comments** |
| ------------- | ------------- | ------------- |
| migration_id | Integer | The id of the migration record, as kept by the DCMC Master Client. |
| form | String | The form of the marketplace on which the market action is relevant. For the IT Load Marketplace, this parameter should be equal to “it_load”|
| timeframe| String | The timeframe of the marketplace on which the relevant market action is meant to be placed (can be “intra_day”, “day_ahead”) for the IT Load Marketplace |
| date | Datetime | The date at which the reference price is desired in iso8601 format |
| action_id | Integer | The id of a market action in the CATALYST IT Load Marketplace |

## Installation
Go to the ITMC source code directory
```bash
$ cd <download_directory>/itl-marketplace-connector
```

Go to the docker directory of the ITMC source
```bash
$ cd docker
```

Edit the configuration file in order to provide the marketplace credentials of the host Data Center in (MARKETPLACE_USERNAME, MARKETPLACE_PASSWORD) and the url of the CATALYST Marketplace Information Broker (MARKETPLACE_IB_BASE_URL). 
Also, edit the DCMCM-related settings to allow token authentication to DCMC.
```bash
$ vim .env
```

Build and start ITMC
In some systems, the command might need to be granted with superuser privileges. Depending on the host operating system, the modification of the following command might change.
```bash
$ docker-compose up --build -d
```

**Prerequisites**
* CATALYST Federated Data Center Migration Controller (DCMC)
* CATALYST Information Broker (IB)
* docker and docker-compose