import logging
import multiprocessing
import os
import random

from flask import request
from flask_api import FlaskAPI, status
from flask_cors import CORS
from jsonschema import validate, FormatChecker, ValidationError

from clients.dcmc.master.dcmcm_api import DCMCMasterClient
from clients.marketplace.client import Client
from clients.marketplace.schemas.marketplace import migration_action as migration_action_schema, \
    action_status as action_status_schema
from utils.Utils import get_timeframe_from_dates

app = FlaskAPI(__name__)
CORS(app)
market_client = None
dcmc_client = None


def _get_marketplace_client():
    """
    Gets a marketplace client
    :return: A marketplace Client object
    """
    global market_client
    if not market_client:
        market_client = Client(username=os.getenv('MARKETPLACE_USERNAME', 'participant1'),
                               password=os.getenv('MARKETPLACE_PASSWORD', 'Marketplace2019'),
                               base_url=os.getenv('MARKETPLACE_IB_BASE_URL', 'http://192.168.1.206:5000'),
                               path=os.getenv("MARKTETPLACE_PATH"),
                               logger=app.logger)
    return market_client


def _get_dcmc_client():
    """
    Gets a DCMC Master client
    :return:
    """
    global dcmc_client
    if not dcmc_client:
        dcmc_client = DCMCMasterClient(dcmcm_host_url=os.getenv('DCMCM_URL'),
                                       username=os.getenv('DCMCM_USERNAME'),
                                       password=os.getenv('DCMCM_PASSWORD'))
    return dcmc_client

def _wait_for_market_clearing(action_id, migration_id):
    """
    Waits for the market to get cleared, then it instructs DCMC to start the migration
    :param action_id: the id of the market action to watch for
    :param migration_id: The id of the migration of interest
    :return: None
    """
    # Wait until the action gets accepted
    # Let's wait for a maximum of 24 hours, namely 86400 seconds
    action_accepted = _get_marketplace_client().check_action(action_id, 
                                                             timeout=86400, 
                                                             sleep=10, 
                                                             valid_statuses=["accepted"],
                                                             invalid_statuses=["rejected"])
    if action_accepted:
        app.logger.info("Action was accepted, informing DCMC")
        res = _get_dcmc_client.migration_update(payload={
        "marketaction": action_id,
        "migration"   : migration_id,
        "status"      : "pending"
        })
    else:
        app.logger.warning("Action was rejected, informing DCMC")
        res = _get_dcmc_client.migration_update(payload={
        "marketaction": action_id,
        "migration"   : migration_id,
        "status"      : "rejected"
        })
        return False
    #Now, go for the migration details
    migration_details = _get_marketplace_client().get_migration_details(action_id)
    if migration_details:
        res = _get_dcmc_client().migration_details(migration_details)
        app.logger.info(f"[DCMC]\tMigration details: {res}")
        return True
    else:
        app.logger.critical("Could not get migration details.")
        return False
    #That's all folks!

def _process_migration_request(migration_id, migration_action, form):
    """
    Processes a migration request and informs the DCMCM about the results
    :param migration_id: The id of the migration
    :param migration_action: The migration action object
    :param form: The marketplace form
    :return: None
    """
    timeframe = get_timeframe_from_dates(submission_time=migration_action['date'],
                                         delivery_time=migration_action['starttime'])
    # Get a valid marketplace
    marketplace = _get_marketplace_client().get_marketplace(form=form, timeframe=timeframe)
    if not marketplace:
        app.logger.critical(f"Could not find valid marketplace of form {form} and timeframe {timeframe}")
        res = _get_dcmc_client().migration_update(payload={
            "marketaction": None,
            "migration"   : migration_id,
            "status"      : "rejected"
        })
        app.logger.debug(res)
        return

    # Get a valid session
    sessions = _get_marketplace_client().get_active_session(marketplace)
    if not sessions:
        app.logger.debug(f'Could not find any valid and active sessions for marketplace {marketplace["id"]}')
        res = _get_dcmc_client().migration_update(payload={
            "marketaction": None,
            "migration"   : migration_id,
            "status"      : "rejected"
        })
        app.logger.debug(res)
        return
    if len(sessions) > 1:
        app.logger.debug(f'Found {len(sessions)} valid and active sessions. Choosing one at random')
    chosen_session = random.choice(sessions)

    # Try to post the action
    actions, errors = _get_marketplace_client().register_action(chosen_session, migration_action)
    if not actions:
        app.logger.critical(f'Could not place market action: {errors}')
        res = _get_dcmc_client().migration_update(payload={
            "marketaction": None,
            "migration"   : migration_id,
            "status"      : "rejected"
        })
        app.logger.debug(res)
        return

    action = actions[0]
    app.logger.debug(
        f"Created action {action['id']} in marketplace {marketplace['id']} of session {chosen_session['id']}")

    # Continuously check whether the action is valid
    action_valid = _get_marketplace_client().check_action(action['id'], timeout=10, sleep=0.2)
    if not action_valid:
        app.logger.debug(f'Could not place valid action to the marketplace, or timeout occurred.')
        # Let the DCMCM know
        res = _get_dcmc_client().migration_update(payload={
            "marketaction": action['id'],
            "migration"   : migration_id,
            "status"      : "rejected"
        })
        app.logger.debug(res)
        return

    # Let the DCMCM know
    res = _get_dcmc_client().migration_update(payload={
        "marketaction": action['id'],
        "migration"   : migration_id,
        "status"      : "posted"
    })
    app.logger.debug(res)

    # Now wait for the market to get cleared
    t = multiprocessing.Process(target=_wait_for_market_clearing, args=(action['id'],
                                                                        migration_id))
    t.start()
    return


def _register_action_status_update(action_id, action_status):
    app.logger.debug(f"Updating DCMCM over migration {action_id}: status is {action_status['status']}")
    updated = _get_marketplace_client().update_action_status(action_id, action_status)
    if updated:
        app.logger.debug(f"Successfuly updated the status of action {action_id} to '{action_status}'")
        return '', status.HTTP_200_OK
    else:
        app.logger.error(f"Could not update the status of action {action_id} to '{action_status}'")
        payload = {"error": f"Could not update the status of action {action_id} to '{action_status}'"}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR


@app.route("/migration/<int:migration_id>/actions/<form>/", methods=['POST'])
def register_migration_action_candidates(migration_id, form):
    """
    Registers a migration action and informs the marketplace and the DCMCM in a subprocess.
    :param migration_id: The migration id
    :param form: The marketplace form of interest (should be "it_load") else it will fail afterwards.
    :return: None
    """
    migration_action = request.json
    try:
        validate(instance=migration_action, schema=migration_action_schema, format_checker=FormatChecker())
        t = multiprocessing.Process(target=_process_migration_request, args=(migration_id, migration_action, form))
        t.start()
        return '', status.HTTP_201_CREATED
    except ValidationError as ve:
        app.logger.error(ve)
        payload = {'error': f'Not a valid market action provided: {str(ve.message)}'}
        return payload, status.HTTP_400_BAD_REQUEST
    except Exception as e:
        app.logger.exception(e)
        payload = {'error': f'A server error occurred: {str(e)}'}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR


@app.route("/migration/<int:migration_id>/actions/<form>/<timeframe>/", methods=['POST'])
def register_migration_action_candidates_for_timeframe(migration_id, form, timeframe):
    """
    Registers a migration action and informs the marketplace and the DCMCM in a subprocess.
    :param migration_id: The migration id
    :param form: The marketplace form of interest (should be "it_load") else it will fail afterwards.
    :param timeframe: The timeframe of the marketplace
    :return: None
    """
    payload = {'message': 'This is deprecated. Use the /migration/<migration_id>/actions/<form> endpoint instead.'}
    return payload, status.HTTP_501_NOT_IMPLEMENTED


@app.route("/actions/<int:action_id>/status/", methods=['POST'])
def register_migration_status_update(action_id):
    action_status = request.json
    try:
        validate(instance=action_status, schema=action_status_schema)
        return _register_action_status_update(action_id, action_status)
        return '', status.HTTP_200_OK
    except ValidationError as ve:
        app.logger.error(ve)
        payload = {'error': f'Not a valid market action provided: {str(ve.message)}'}
        return payload, status.HTTP_400_BAD_REQUEST
    except Exception as e:
        app.logger.exception(e)
        payload = {'error': f'A server error occurred: {str(e)}'}
        return payload, status.HTTP_500_INTERNAL_SERVER_ERROR


###############
# PRICES APIs #
###############

@app.route("/prices/<form>/<timeframe>/", methods=['GET'])
def retrieve_prices(form, timeframe):
    price = _get_marketplace_client().get_price(form, timeframe, None)
    if price and 'price' in price:
        price = {
            'price': float("{0:.2f}".format(float(price['price'])))
        }
        return price, status.HTTP_200_OK
    else:
        payload = {'error': f'Could not find price for {form}, {timeframe}'}
        return payload, status.HTTP_404_NOT_FOUND


@app.route("/prices/<form>/<timeframe>/date/<date>/", methods=['GET'])
def retrieve_prices_for_date(form, timeframe, date):
    price = _get_marketplace_client().get_price(form, timeframe, date)
    if price and 'price' in price:
        price = {
            'price': float("{0:.2f}".format(float(price['price'])))
        }
        return price, status.HTTP_200_OK
    else:
        payload = {'error': f'Could not find price for {form}, {timeframe}'}
        return payload, status.HTTP_404_NOT_FOUND


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
else:
    # Running via gunicorn (production)
    glogger = logging.getLogger('gunicorn.error')
    app.logger.handlers = glogger.handlers
    app.logger.setLevel(glogger.level)
