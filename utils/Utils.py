from dateutil.parser import parse


def get_timeframe_from_dates(submission_time, delivery_time):
    """
    Gets the timeframe of a candidate marketplace based on the difference between the delivery time and the submission
    time
    :param submission_time: The submission time of the action
    :param delivery_time: The delivery time of the action
    :return: The timeframe. Can be either 'day_ahead', 'intra_day' or None.
    """
    sdate = parse(submission_time).date()
    ddate = parse(delivery_time).date()
    if sdate == ddate:
        return 'intra_day'
    elif (ddate - sdate).days == 1:
        return 'day_ahead'
    else:
        return None

