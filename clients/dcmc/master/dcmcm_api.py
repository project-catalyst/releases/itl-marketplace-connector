import os
from urllib.parse import urljoin

import requests

from clients.dcmc.client import Client


class DCMCMasterClient(object):
    """DCMC Master Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Master
    (DCMCM) Client component of the H2020 CATALYST Migration Controller. It covers
    calls to the entire available API of the DCMC Master Client API.

    Methods
    -------
    controller()
        Get the OpenStack Controller details
    migration_by_marketaction(marketaction)
        Retrieve migration by the associated marketaction ID
    migration_request(payload)
        Issue a migration request to the DCMC Master.
    migration_details(payload)
        Send the migration details to the DCMC Master.
    migration_update(payload)
        Update the status of a migration.
    vcontainer(payload)
        Issue request for VC creation to the DCMC Master.
    vcontainer_by_uuid(uuid)
        Retrieve a VContainer by its UUID
    vcontainer_registered(transaction_id)
        Inform DCMC Master of VContainer's registration.

    """
    __API_PREFIX = 'catalyst/dcmcm/api/'
    __CONTROLLER = 'controller/'
    __MIGRATION_BY_MARKETACTION = 'migration/marketaction/{}/'
    __MIGRATION_DETAILS = 'migration/details/'
    __MIGRATION_REQUEST = 'migration/request/'
    __MIGRATION_UPDATE = 'migration/marketplace/update/'
    __VCONTAINER = 'vcontainer/'
    __VCONTAINER_BY_UUID = 'transaction/vcontainer/{}/'
    __VCONTAINER_REGISTERED = 'transaction/{}/vcontainer/status/registered/'

    def __init__(self, dcmcm_host_url, username, password):
        """DCMC Master Client Class Constructor.

        Parameters
        ----------
        dcmcm_host_url : URL
            The DCMC Master's Host URL
        username : str
            The username for the DCMC Master
        password : str
            The password for the DCMC Master

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin(dcmcm_host_url, DCMCMasterClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.__token = self.__get_keycloak_token(username, password)
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    @staticmethod
    def __get_keycloak_token(username, password):
        """Get an access token from Keycloak for authorization on the DCMC Master.

        Parameters
        ----------
        username : str
            A Keycloak Username
        password : str
            A Keycloak user's password

        Returns
        -------
        token : str
            A Keycloak token if request is valid, else None

        """
        payload = {
            'username': username,
            'password': password,
            'grant_type': 'password',
            'client_id': os.getenv('OAUTH_CLIENT_ID'),
            'client_secret': os.getenv('OAUTH_CLIENT_SECRET')
        }
        params = {'realm-name': os.getenv('OAUTH_REALM')}
        url = urljoin(os.getenv('OAUTH_URL'), os.getenv('OAUTH_USER_TOKEN')).format(**params)
        response = requests.post(url=url, data=payload)
        return response.json()['access_token'] if response.status_code == 200 else None

    def controller(self):
        """Fetch the OpenStack Controller details.

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__CONTROLLER)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def migration_by_marketaction(self, marketaction):
        """Retrieve migration by marketaction ID.

        Parameters
        ----------
        marketaction : int
            The ID of the marketaction to fetch migration for.

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_BY_MARKETACTION).format(marketaction)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def migration_details(self, payload):
        """Send Migration Details to DCMCM.

        When the migration details, including source, destination, start time, end time etc.
        are received by the DCMC Master, the essential pre-migration procedures are executed.

        Parameters
        ----------
        payload : dict
            The payload to send to the migration details endpoint. Includes the following keys:
                marketaction : int
                    The ID of the marketaction (IT Load Marketplace) corresponding to the migration
                source : str
                    The username of the migration's source DC as understood by the DCMC Server
                destination : str
                    The username of the migration's destination DC as understood by the DCMC Server
                delivery_start : str
                    The datetime on which the migration is planned to start
                delivery_end : str
                    The datetime on which the migration is planned to end
                transaction : int
                    The ID of the transaction as kept in the Catalyst IT Load Marketplace

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_DETAILS)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def migration_request(self, payload):
        """Send migration request to DCMC Master.

        An initial migration request is issued by the Energy Aware IT Load Balancer to each
        of DCMC masters that are going to take part in the inter-DC migration, including
        information about the load, start and end times, etc.

        Parameters
        ----------
        payload : dict
            The payload to send to the migration request endpoint. Includes the following parameters:
                date : datetime
                    The time on which the request is created
                vc_tag : str
                    The VC Tag as generated by the VCG
                starttime : datetime
                    The time on which the requested migration is planned to start
                endtime : datetime
                    The time on which the requested migration is planned to end
                load_values : dict
                    Information about the load characteristics. Includes the following fields:
                        parameter : str
                            The name of the load characteristic
                        value : float
                            The value quantifying the load characteristics
                        uom : str
                            The unit of measurement of the value
                price : float
                    The price suggested for the migration action
                action_type : {'bid', 'offer'}
                    The type of market action which will be placed on the IT Load Marketplace

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_REQUEST)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def migration_update(self, payload):
        """Update the marketaction and status of the migration.

        On the first call to the migration update endpoint the marketaction is a mandatory
        argument as the migration and marketaction have not been associated yet. After the
        first call, the marketaction parameter can be omitted.

        Parameters
        ----------
        payload : dict
            The payload to send to the migration update endpoint. Includes the following fields:
                marketaction : int
                    The ID of the marketaction (IT Load Marketplace) corresponding to the migration
                migration : int
                    The ID of the migration
                status : {'pending', 'posted', 'rejected', 'started', 'success', 'failed'}
                    The status of the migration.

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_UPDATE)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def vcontainer(self, payload):
        """Send request for VContainer Initiation.

        The DCMC Server issues this request to the DCMC Master of the destination DC to
        trigger the creation of the VContainer, intended to host the OpenStack virtual
        compute node (vCMP) for the IT Load of the source DC to be migrated to.

        Parameters
        ----------
        payload : dict
            The payload to send to the vcontainer endpoint. Includes the following fields:
                cpu : int
                    The number of CPUs of the described VC
                cpu_uom : str
                    The unit of measurement for the CPU
                ram : int
                    The amount of RAM of the described VC
                ram_uom : str
                    The unit of measurement for the RAM value
                disk : int
                    The amount of disk of the described VC
                disk_uom : str
                    The unit of measurement for the disk value
                start : str
                    The datetime from which the VC is planned to be active
                end : str
                    The datetime until which the VC is planned to be active

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__VCONTAINER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def vcontainer_by_uuid(self, uuid):
        """Get a VContainer by its UUID.

        Parameters
        ----------
        uuid : str
            The UUID of the VContainer to fetch

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__VCONTAINER_BY_UUID).format(uuid)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def vcontainer_registered(self, transaction_id):
        """Inform DCMCM of VContainer registration.

        The DCMC Lite Client informs the DCMC Master of the source DC that the creation
        of the VContainer has been completed and the virtual OpenStack Compute Node (vCMP)
        has been registered with the controller of the source DC.

        Parameters
        ----------
        transaction_id : int
            The transaction ID associated with the registered VContainer

        Returns
        -------
        Response
            The Response object of the issued request

        """
        url = urljoin(self.__url, DCMCMasterClient.__VCONTAINER_REGISTERED).format(transaction_id)
        response = self.__client.post(url=url, headers=self.__headers, payload={})
        return response
