import json
import random
import time
from datetime import datetime
from urllib.parse import urljoin

import requests


class Client(object):

    def __init__(self, username, password, base_url, path, logger):
        self.username = username
        self.password = password
        self.base_url = base_url
        self.log = logger
        self.token = None
        self.actor = None
        self.path = path
        self.get_token()

    def get_token(self):
        """
        Gets a token from the marketplace
        :return:
        """
        if not self.token:
            relative_url = self.path + '/tokens/'
            url = urljoin(self.base_url, relative_url)
            payload = {
                'username': self.username,
                'password': self.password
            }
            res = requests.post(url=url,
                                json=payload)
            if res.status_code == 200:
                self.token = res.json()['token']
                self.actor = res.json()['actor']['id']
                self.log.debug(f"Registered marketplace token {self.token}")
            else:
                self.log.critical('Could not get a token ({}): {}'.format(res.status_code, res.text))
        return self.token

    def get_price(self, form, timeframe, date):
        """
        Gets the price for a particular timeframe and form
        :param form: The marketplace form of interest
        :param timeframe: The timeframe of interest
        :param date: The date of interest. If None, then the present date is assumed
        :return: The price object of interest
        """
        marketplace = self.get_marketplace(form=form, timeframe=timeframe)
        if not marketplace:
            return None

        if not date:
            date = 'now'
            self.log.debug(f"Date not set. Setting date to '{date}'.")
        relative_url = self.path + f"/marketplace/{marketplace['id']}/form/{form}/date/{date}/referencePrice/"
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                           headers={
                               "Accept"       : "application/json",
                               "Authorization": f"Token {self.get_token()}"
                           })
        if res.status_code == 200:
            return res.json()
        else:
            self.log.warning(f"Could not find reference price for {form}, {timeframe}, {date}")
            self.log.warning(f"Tried GET {url} and got ({res.status_code}) - {res.text}")
            return None

    def get_marketplace(self, form, timeframe):
        """
        Gets a marketplace for a form and timeframe
        :param form: The marketplace energy form
        :param timeframe: The marketplace timeframe
        :return: A matching marketplace if one exists, else None
        """
        relative_url = self.path + f"/marketplace/form/{form}/timeframe/{timeframe}/"
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                           headers={
                               "Accept"       : "application/json",
                               "Authorization": f"Token {self.get_token()}"
                           })
        if res.status_code == 200:
            marketplaces = res.json()
            if len(marketplaces) == 0:
                self.log.error(f'Could not find any valid marketplace of form {form} and timeframe {timeframe}.')
                self.log.debug(f'Tried {url}.')
                return None
            else:
                # Return a random one
                marketplace = random.choice(marketplaces)
                self.log.debug(
                    f"Found {len(marketplaces)} candidate marketplaces. Choosing marketplace with id {marketplace['id']}.")
                return marketplace
        else:
            self.log.error(f'Could not find a valid marketplace of form {form} and timeframe {timeframe}.')
            self.log.error(f'({res.status_code}) - {res.text}')
            return None

    def get_active_session(self, marketplace):
        if not marketplace:
            return None
        relative_url = self.path + f"/marketplace/{marketplace['id']}/marketsessions/active/"
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                           headers={
                               "Accept"       : "application/json",
                               "Authorization": f"Token {self.get_token()}"
                           })
        if res.status_code == 200:
            sessions = res.json()
            if len(sessions) > 0:
                return sessions
            else:
                return None
        else:
            self.log.error(f"Could not find an active session of marketplace {marketplace['id']}.")
            self.log.error(f'({res.status_code}) - {res.text}')
            return None

    def get_form(self, form):
        """
        Gets a maketplace form
        :param form: The form to get information for
        :return:
        """
        relative_url = self.path + "/form/"
        url = urljoin(self.base_url, form)
        res = requests.get(url=url,
                           headers={
                               "Authorization": f"Token {self.token}"
                           })
        if res.status_code != 200:
            self.log.error(f'Could not retrieve forms ({res.status_code})')
            return None
        for _form in res.json():
            if _form['form'] == form:
                return _form

    def get_action_type_id(self, action_type):
        relative_url = self.path + '/actiontypes/'
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                           headers={
                               "Authorization": f"Token {self.token}"
                           })
        if res.status_code != 200:
            self.log.error(f'Could not retrieve action types: ({res.status_code}) {res.text}')
            return None
        for _action_type in res.json():
            if _action_type['type'] == action_type:
                return _action_type['id']

    def get_action_status(self, action_status):
        relative_url = self.path + '/actionstatus/'
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                           headers={
                               "Authorization": f"Token {self.token}"
                           })
        if res.status_code != 200:
            self.log.error(f'Could not retrieve action statuses: ({res.status_code}) {res.text}')
            return None
        for _action_status in res.json():
            if _action_status['status'] == action_status['status']:
                return _action_status

    def register_load(self, date, load):
        """
        :param load: The load to register (see definition of LoadValue in D3.3 and in clients.marketplace.schemas.marketplace).
        :return: load_id, {"cpu": <cpu_cores>, "ram": <ram>, "disk": <disk>}
        """
        loads_relative_url = self.path + '/load/'
        loads_url = urljoin(self.base_url, loads_relative_url)
        loads_payload = {"date": date}
        loads_res = requests.post(url=loads_url,
                                  headers={
                                      "Content-Type" : "application/json",
                                      "Authorization": f"Token {self.token}"
                                  },
                                  json=loads_payload)
        if loads_res.status_code != 201:
            self.log.error(f"Could not register load: ({loads_res.status_code}) {loads_res.text}")
            return None
        load_id = loads_res.json()['id']

        load_values_relative_url = self.path + '/loadvalues/'
        load_values_url = urljoin(self.base_url, load_values_relative_url)
        params = {"cpu": None, "ram": None, "disk": None}
        for load_value in load:
            payload = load_value
            payload['loadid'] = load_id
            payload['parameter'] = self._transform_parameter(payload['parameter'])
            payload['uom'] = self._transform_uom(payload['uom'])
            load_value_res = requests.post(url=load_values_url,
                                           headers={
                                               "Content-Type" : "application/json",
                                               "Authorization": f"Token {self.token}"
                                           },
                                           json=payload)
            if load_value_res.status_code != 201:
                self.log.error(
                    f"Could not register load value {load_value}: ({load_value_res.status_code}) {load_value_res.text}")
                return None
            params[payload['parameter']] = payload['value']

        return load_id, params

    def register_action(self, session, migration_action):
        """
        Registers a migration action to the marketplace
        :param session: The marketplace session
        :param migration_action: The migration action to register
        :return: Market_Action, Errors
        """
        marketplace_id = session['marketplaceid']['id']
        session_id = session['id']
        form_id = session['formid']['id']
        load_id, params = self.register_load(migration_action['date'], migration_action['loadvalues'])
        action_type = self.get_action_type_id(migration_action['action_type'])
        status_id = 1  # It should not matter. If it matters, the IB API should change.
        payload = [{
            "date": migration_action['date'],
            "actionStartTime": migration_action['starttime'],
            "actionEndTime": migration_action['endtime'],
            "actionTypeid": action_type,
            "statusid": status_id,
            "loadid": load_id,
            "cpu": params['CPU'] if params else None,
            "ram": params['RAM'] if params else None,
            "disk": params['Disk'] if params else None,
            "deliveryPoint": "",
            "formid": form_id,
            "marketActorid": self.actor,
            "marketSessionid": session_id,
            "price": migration_action['price'],
            "value": migration_action["value"],
            "uom": migration_action["uom"]
        }]
        relative_url = self.path + f'/marketplace/{marketplace_id}/marketsessions/{session_id}/actions/'
        url = urljoin(self.base_url, relative_url)
        res = requests.post(url=url,
                            headers={
                                "Content-Type" : "application/json",
                                "Authorization": f"Token {self.token}"
                            },
                            json=payload)
        if res.status_code != 201:
            message = f"Could not place action {payload}: ({res.status_code}) {res.text}"
            return None, message
        return res.json(), None

    def check_action(self, action_id, timeout=10, sleep=0.1, statuses=["valid"], invalid_statuses=["invalid"]):
        """
        Checks an action for its status
        :param action_id: The id of the action to check
        :param timeout: The period for which to continue cheking in seconds
        :param sleep: Time interval between successive checks, in seconds
        :param statuses: The list of valid statuses of the action
        :return: True if in the end we got a status falling under the statuses list 
        """
        start = datetime.utcnow()
        relative_url = self.path + f"/action/{action_id}/"
        url = urljoin(self.base_url, relative_url)
        while(True):
            time.sleep(sleep)
            now = datetime.utcnow()
            if (now - start).total_seconds() > timeout:
                break
            headers = {
                "Authorization": f"Token {self.token}"
            }
            res = requests.get(url=url,
                                headers=headers)
            if res.status_code != 200:
                self.log.debug(f"Could not check for status of action {action_id}. ({res.status_code}) {res.text}")
                self.log.debug(f"URL    : {url}")
                self.log.debug(f"Headers: {headers}")
                break
            if res.json()['statusid']['status'] in valid_statuses:
                self.log.debug(f"Action {action_id} is {res.json()['statusid']['status']}.")
                return True
            elif res.json()['statusid']['status'] in invalid_statuses:
                self.log.debug(f"Action {action_id} is {res.json()['statusid']['status']}")
                return False
            else:
                self.log.debug(f"Action {action_id} is {res.json()['statusid']['status']}. Waiting..")
        return False

    def update_action_status(self, action_id, action_status):
        # Get the status
        status = self.get_action_status(action_status)
        if not status:
            self.log.info(f"Could not resolve status {action_status}")
            return False
        # Get the action
        relative_url = self.path + f"/action/{action_id}/"
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                             headers= {
                                  "Authorization": f"Token {self.token}"
                             })
        if res.status_code != 200:
            self.log.error(f"Could not update status of action {action_id} to {action_status}")
            self.log.error(f"({res.status_code}) - {res.text}")
            return False

        action = res.json()
        if not action:
            self.log.error("The action has been  nullified")
            return False

        # SET the necessary stuff first
        marketplace_id = action['marketSessionid']['marketplaceid']['id']
        marketplace_id = action['marketSessionid']['marketplaceid']['id']
        session_id = action['marketSessionid']['id']

        # Now update the action model
        action['actionTypeid'] = action['actionTypeid']['id']
        action['formid'] = action['formid']['id']
        action['loadid'] = action['loadid']['id']
        action['marketActorid'] = action['marketActorid']['id']
        action['marketSessionid'] = action['marketSessionid']['id']

        action['statusid'] = status['id']

        relative_url = self.path + f"/marketplace/{marketplace_id}/marketsessions/{session_id}/actions/{action_id}/edit/"
        url = urljoin(self.base_url, relative_url)
        res = requests.put(url=url,
                           headers={
                               "Authorization": f"Token {self.token}"
                           },
                           json=action)
        if res.status_code == 200:
            self.log.info(f"Updated action {action_id} to status '{action_status}'")
            return True
        else:
            self.log.info(f"Could not action {action_id} to status '{action_status}'")
            self.log.info(f"({res.status_code}) - {res.text}")
            return False

    def get_migration_details(self, action_id):
        """
        Gets the migration details of an action
        :param action_id: The action of interest
        :return: The migration details object if ok, else None
        """
        relative_url = self.path + f"/marketactions/{action_id}/migration/details/"
        url = urljoin(self.base_url, relative_url)
        res = requests.get(url=url,
                           headers= {
                               "Authorization": f"Token {self.token}"
                           })
        if res.status_code != 200:
            self.log.error(f"Could not get migration details for action {action_id}")
            self.log.error(f"({res.status_code}) - {res.text}")
            return None
        return res.json()

    def _transform_parameter(self, param):
        if param == 'cpu':
            return 'CPU'
        elif param == 'ram':
            return 'RAM'
        elif param == 'disk':
            return 'Disk'
        else:
            return None

    def _transform_uom(self, uom):
        if uom == 'cpu':
            return 'core'
        elif uom == 'MB':
            return 'MB'
        elif uom == 'GB':
            return 'GB'

