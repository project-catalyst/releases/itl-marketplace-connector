migration_action = {
    "type": "object",
    "properties": {
        "date": {
            "type": "string",
            "format": "date-time"
        },
        "starttime": {
            "type": "string",
            "format": "date-time"
        },
        "endtime": {
            "type": "string",
            "format": "date-time"
        },
        "loadvalues": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/loadvalue"
            }
        },
        "value": {
            "type": "number",
            "multipleOf": 0.01
        },
        "uom": {
            "type": "string"
        },
        "price": {
            "type": "number",
            "multipleOf": 0.01
        },
        "action_type": {
            "type": "string",
            "enum": ["bid", "offer"]
        }
    },
    "required": ["date", "starttime", "endtime", "loadvalues", "value", "uom", "price", "action_type"],
    "definitions": {
        "loadvalue": {
            "type": "object",
            "properties": {
                "parameter": {
                    "type": "string",
                    "enum": ["cpu", "ram", "disk"]
                },
                "value": {
                    "type": "number"
                },
                "uom": {
                    "type": "string",
                    "enum": ["cpu", "MB", "GB"]
                }
            },
            "required": ["parameter", "value", "uom"]
        }
    }
}

action_status = {
    "type": "object",
    "properties": {
        "status": {
            "type": "string",
            "enum": ["unchecked", "valid", "invalid", "active", "inactive", "withdrawn", "accepted",
                     "partially_accepted", "rejected", "selected", "delivered", "not_delivered", "billed"]
        }
    },
    "required": ["status"]
}